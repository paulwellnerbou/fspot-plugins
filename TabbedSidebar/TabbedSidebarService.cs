/*
 * TabbedSidebarService.cs
 *
 * Author(s)
 * 	Paul Wellner Bou <paul@purecodes.org>
 *
 * This is free software. See COPYING for details
 */

using System;
using FSpot;
using Gtk;
using FSpot.Extensions;
using FSpot.Utils;
using FSpot.Widgets;

namespace TabbedSidebarService
{
	public class TabbedSidebarService : IService
	{
		Sidebar sidebar;
		RcStyle style;
		
		public bool Start()
		{
			uint timer = Log.InformationTimerStart ("Starting TabbedSidebarService");
			
			sidebar = MainWindow.Toplevel.Sidebar;
			int border_to_add = Gtk.Rc.GetStyle (sidebar.Notebook).XThickness;
			
			style = new RcStyle ();
			style.Xthickness = 0;
			style.Ythickness = 0;
			style.Name = "no-padding";
			sidebar.Notebook.ModifyStyle (style);
			
			// add the missing padding to the tab border
			sidebar.Notebook.TabHborder = sidebar.Notebook.TabHborder + (uint) border_to_add;
			sidebar.Notebook.TabVborder = sidebar.Notebook.TabVborder + (uint) border_to_add;
			
			sidebar.Notebook.ShowTabs = true;
			sidebar.Notebook.Scrollable = true;
			sidebar.Notebook.EnablePopup = true;
			sidebar.Notebook.BorderWidth = 0;
			sidebar.Notebook.SwitchPage += new SwitchPageHandler(this.OnNotebookSwitchPage);
			sidebar.Notebook.PageAdded += new PageAddedHandler(this.OnNotebookAddedPage);
			
			int n_pages = sidebar.Notebook.NPages;
			for (int i = 0; i < n_pages; i++) {
				Widget pageWidget = sidebar.Notebook.GetNthPage (i);
				adjustPageWidget (pageWidget);
			}
			
			// Hiding eventBox (tabs do replace the popup menu)
			sidebar.Children [0].Visible = false;
			
			Log.DebugTimerPrint (timer, "TabbedSidebarService startup took {0}");
			return true;
		}
		
		private void adjustPageWidget(Widget pageWidget)
		{
			// Adjust border of existing notebook pages
			if (pageWidget is ScrolledWindow) {
				ScrolledWindow sw = (ScrolledWindow) pageWidget;
				sw.ShadowType = ShadowType.In;
				sw.BorderWidth = 0;
				if (sw.Children.Length == 1 && sw.Children [0] is Viewport) {
					Viewport w = (Viewport) sw.Children [0];
					w.ShadowType = ShadowType.None;
				}
				
			} else if (pageWidget is Viewport) {
				Viewport w = (Viewport) pageWidget;
				w.ShadowType = ShadowType.In;
				w.BorderWidth = 0;
			}
		}
		
		public void OnNotebookAddedPage (object obj, PageAddedArgs args)
		{
			adjustPageWidget (args.P0);
		}
		
		public void OnNotebookSwitchPage (object obj, SwitchPageArgs args)
		{
			int n = (int) args.PageNum;
			
			/* sidebar
			 * 	`-- (Gtk.HBox) buttonbox
			 * 		`-- (Gtk.EventBox) eventbox
			 * 			`-- (FSpot.Widgets.MenuButton) choosebutton
			 * 				`-- (Gtk.Menu) menu
			 */
			Gtk.Menu menu = (((sidebar.Children [0] as Gtk.HBox).Children [0] as Gtk.EventBox).Children [0] as FSpot.Widgets.MenuButton).Menu;
			
			// This does not seem to work the Activated handler of the MenuItems
			menu.SetActive ((uint) n);
			
			// Thus, calling the handler separately.
			// This is an ugly workaround. I can not do what I would need to do here,
			// as ContextStrategy is not public.
			// TODO: This should be done in the Switch(int) method within Sidebar.
			MainWindow.Toplevel.Sidebar.HandleItemClicked (menu.Children [n], null);
		}

		public bool Stop ()
		{
			uint timer = Log.InformationTimerStart ("Stopping TabbedSidebarService");
			Log.DebugTimerPrint (timer, "TabbedSidebarService shutdown took {0}");
			return true;
		}
	}
}

