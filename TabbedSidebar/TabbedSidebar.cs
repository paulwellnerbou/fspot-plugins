/*
 * TabbedSidebarService.cs
 *
 * Author(s)
 * 	Paul Wellner Bou <paul@purecodes.org>
 *
 * This is free software. See COPYING for details
 */

using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using Gtk;
using Gdk;
using FSpot;
using FSpot.Utils;
using TabbedSidebar;

using Mono.Unix;

namespace FSpot.Widgets {
	public class TabbedSidebarPage : SidebarPage {
		public TabbedSidebarPage () : base(new TabbedSidebarWidget (), 
											Catalog.GetString ("Tabbed Sidebar"), 
											"gtk-index") {
			(SidebarWidget as TabbedSidebarWidget).Page = this;
		}

		TabbedSidebarWidget widget;
		
		protected override void AddedToSidebar ()
		{
			widget = SidebarWidget as TabbedSidebarWidget;
			widget.ParentSet += delegate (object sender, Gtk.ParentSetArgs e) {
				widget.createTabsOnNotebook ();
				Notebook notebook = widget.Parent as Notebook;
				notebook.Realized += delegate (object o, System.EventArgs args) {
					TabbedSidebarRemover t = new TabbedSidebarRemover ();
					t.removeTabbedSidebarFromNotebook (notebook);
				};
			};
		}
		
		public void OnNotebookAddedPage (object obj, PageAddedArgs args)
		{
			Sidebar sidebar = widget.Parent.Parent as Sidebar;
			adjustPageWidget (args.P0);
		}
		
		public void OnNotebookSwitchPage (object obj, SwitchPageArgs args)
		{
			Sidebar sidebar = widget.Parent.Parent as Sidebar;
			int n = (int) args.PageNum;
			
			/* sidebar
			 * 	`-- (Gtk.HBox) buttonbox
			 * 		`-- (Gtk.EventBox) eventbox
			 * 			`-- (FSpot.Widgets.MenuButton) choosebutton
			 * 				`-- (Gtk.Menu) menu
			 */
			Gtk.Menu menu = (((sidebar.Children [0] as Gtk.HBox).Children [0] as Gtk.EventBox).Children [0] as FSpot.Widgets.MenuButton).Menu;
			
			// This does not seem to work the Activated handler of the MenuItems
			menu.SetActive ((uint) n);
			
			// Thus, calling the handler separately.
			// This is an ugly workaround. I can not do what I would need to do here,
			// as ContextStrategy is not public.
			// TODO: This should be done in the Switch(int) method within Sidebar.
			if (n < menu.Children.Length)
				sidebar.HandleItemClicked (menu.Children [n], null);
		}
		
		public void adjustPageWidget(Widget pageWidget)
		{
			// Adjust border of existing notebook pages
			if (pageWidget is ScrolledWindow) {
				ScrolledWindow sw = (ScrolledWindow) pageWidget;
				sw.ShadowType = ShadowType.In;
				sw.BorderWidth = 0;
				if (sw.Children.Length == 1 && sw.Children [0] is Viewport) {
					Viewport w = (Viewport) sw.Children [0];
					w.ShadowType = ShadowType.None;
				}
				
			} else if (pageWidget is Viewport) {
				Viewport w = (Viewport) pageWidget;
				w.ShadowType = ShadowType.In;
				w.BorderWidth = 0;
			}
		}
	}
	
	public class TabbedSidebarWidget : ScrolledWindow {
		RcStyle style;
		Sidebar sidebar;
		
		public TabbedSidebarPage Page;
		
		public void removeMeFromNotebook ()
		{
			Notebook notebook = this.Parent as Notebook;
			int n = notebook.PageNum (this);
			notebook.RemovePage (n);
		}
		
		public void createTabsOnNotebook ()
		{
			sidebar = this.Parent.Parent as Sidebar;
			int border_to_add = Gtk.Rc.GetStyle (sidebar.Notebook).XThickness;
			
			style = new RcStyle ();
			style.Xthickness = 0;
			style.Ythickness = 0;
			style.Name = "no-padding";
			sidebar.Notebook.ModifyStyle (style);
			
			// add the missing padding to the tab border
			sidebar.Notebook.TabHborder = sidebar.Notebook.TabHborder + (uint) border_to_add;
			sidebar.Notebook.TabVborder = sidebar.Notebook.TabVborder + (uint) border_to_add;
			
			sidebar.Notebook.ShowTabs = true;
			sidebar.Notebook.Scrollable = true;
			sidebar.Notebook.EnablePopup = true;
			sidebar.Notebook.BorderWidth = 0;
			// sidebar.Notebook.SwitchPage += new SwitchPageHandler(Page.OnNotebookSwitchPage);
			// sidebar.Notebook.PageAdded += new PageAddedHandler(Page.OnNotebookAddedPage);
			
			int n_pages = sidebar.Notebook.NPages;
			for (int i = 0; i < n_pages; i++) {
				Widget pageWidget = sidebar.Notebook.GetNthPage (i);
				Page.adjustPageWidget (pageWidget);
			}
			
			// Hiding eventBox (tabs do replace the popup menu)
			sidebar.Children [0].Visible = false;
		}
	}
}

