/*
 * SendToBibbleWorkQueue.cs
 *
 * Author(s)
 * 	Paul Wellner Bou <paul@purecodes.org>
 *
 * This is free software. See COPYING for details
 */

using System;
using System.IO;

using Mono.Unix;
using Hyena;

using FSpot;
using FSpot.Utils;
using FSpot.Extensions;
using FSpot.Imaging;

namespace SendToBibbleWorkQueueExtension
{
	public class SendToBibbleWorkQueue : ICommand
	{
		public void Run (object o, EventArgs e)
		{
            Log.Information ("[SendToBibbleWorkQueue] Executing extension");

			string sep = Path.DirectorySeparatorChar.ToString ();
			string bibbleWorkQueueFileName = UnixEnvironment.EffectiveUser.HomeDirectory
				+ sep + ".bibble" + sep + "work" + sep + "F-Spot.work";
			
			if (!File.Exists (bibbleWorkQueueFileName)) {
				if (!CreateQueueFile (bibbleWorkQueueFileName))
					return;
			}
			
			StreamWriter wr;
			wr = File.AppendText (bibbleWorkQueueFileName);
		
			foreach (Photo p in App.Instance.Organizer.SelectedPhotos ()) {
                PhotoVersion pv = p.GetVersion (Photo.OriginalVersionId) as PhotoVersion;
                Log.Information ("[SendToBibbleWorkQueue] " + pv.Uri.AbsolutePath);
                if (!ImageFile.IsRaw (pv.Uri))
                    Log.Warning ("[SendToBibbleWorkQueue] The original version of this image is not a (supported) RAW file, opening anyway.");

				wr.WriteLine (pv.Uri.AbsolutePath);
			}
			
			wr.Close ();
		}
		
		public bool CreateQueueFile(string fileName)
		{
			try {
				StreamWriter wr = new StreamWriter (File.Create (fileName));
				wr.WriteLine ("F-Spot");
				wr.WriteLine ("");
				wr.Close ();
				return true;
			} catch (Exception e) {
				Log.Error ("[SendToBibbleWorkQueue] " + e.Message);
				Log.Error ("[SendToBibbleWorkQueue] Bibble 4 Professional installed?");
			}
			return false;
		}	
	}
}
