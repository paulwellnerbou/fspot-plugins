MAKE = make
# SUBDIRS = DateTimeFormat ImportRawProcessed SendToBibbleQueue VersionSidebar TabbedSidebar
SUBDIRS = DateTimeFormat ImportRawProcessed DevelopInBibble5 SendToBibbleQueue

all:
	@for i in $(SUBDIRS); do \
	echo "make all in $$i..."; \
	(cd $$i; $(MAKE) all); done

install:
	@for i in $(SUBDIRS); do \
	echo "Installing in $$i..."; \
	(cd $$i; $(MAKE) install); done
	
uinstall:
	@for i in $(SUBDIRS); do \
	echo "Uninstalling in $$i..."; \
	(cd $$i; $(MAKE) uinstall); done

clean:
	@for i in $(SUBDIRS); do \
	echo "Cleaning in $$i..."; \
	(cd $$i; $(MAKE) clean); done
	rm -f *.dll *~ *.bak *.mpack *.mrep index.html

mpack:
	@for i in $(SUBDIRS); do \
	echo "MPacking in $$i..."; \
	(cd $$i; $(MAKE) mpack); done

rep:
	rm -r repo;	mkdir repo
	cd repo; find ../ -name '*.mpack' -exec cp {} . \;
	cd repo; mautil rb .
	echo "<?php echo symlink('0.8', 'latest'); ?>" > create_symlink.php
	echo "Done"