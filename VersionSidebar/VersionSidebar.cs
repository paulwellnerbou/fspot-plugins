/*
 * VersionSidebar.cs
 *
 * Author(s)
 * 	Paul Wellner Bou <paul@purecodes.org>
 *
 * This is free software. See COPYING for details.
 */

using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using Gtk;
using Gdk;
using Hyena;

using FSpot.Widgets;

using Mono.Unix;

namespace FSpot.Extensions {
	public class VersionSidebarPage : SidebarPage {
		public VersionSidebarPage () : base(new VersionSidebarWidget (), 
											Catalog.GetString ("Versions"), 
											"gtk-index") {
			(SidebarWidget as VersionSidebarWidget).Page = this;
		}

		protected override void AddedToSidebar ()
		{
			VersionSidebarWidget widget = SidebarWidget as VersionSidebarWidget;
			(Sidebar as Sidebar).SelectionChanged += widget.HandleSelectionChanged;
		}
	}

	public class VersionSidebarWidget : ScrolledWindow {
		FSpot.Widgets.IconView view;
		PhotoList versions;
		VBox main_vbox;
		ScrolledWindow thumb_scrolledwindow;
		
		public VersionSidebarPage Page;
		
		IBrowsableItem item;
		public IBrowsableItem SelectedItem {
			get {
				return item;
			}
			set {
				item = value;

				if (item != null) {
					ShowPhotoVersions ();
				} else {
					ClearPhotoVersionViews ();
				}
			}
		}
		
		public VersionSidebarWidget ()
		{
			main_vbox = new VBox ();
			main_vbox.Spacing = 6;
			
			versions = new PhotoList ();
			thumb_scrolledwindow = new ScrolledWindow();
			thumb_scrolledwindow.BorderWidth = 0;
			
			main_vbox.Add (thumb_scrolledwindow);
			AddWithViewport (main_vbox);
			
			ShadowType = ShadowType.None;
			BorderWidth = 0;
		}
		
		internal void HandleSelectionChanged (IBrowsableCollection collection) {
			if (collection != null && collection.Count == 1)
				SelectedItem = collection [0];
			else
				SelectedItem = null;
		}
		
		void ShowPhotoVersions ()
		{
		    try {
		        ClearPhotoVersionViews ();
		        if (SelectedItem == null)
		            return;
		  
				FSpot.Photo photo = (FSpot.Photo)SelectedItem;
		        uint[] version_ids = photo.VersionIds;
		        IBrowsableItem[] items = new IBrowsableItem[version_ids.Length];
		
				int i = items.Length;
		        int selected_version_id = -1;
		        foreach (uint id in version_ids) {
		            i--;
		            Log.Information (string.Format("Adding version {0} to items[{1}].", id, i));
		            items[i] = (IBrowsableItem)(photo.GetVersion (id));
					if (id == photo.DefaultVersionId)
						selected_version_id = i;
				}
				
				versions = new PhotoList ();
				versions.Add (items);
				
				view = new FSpot.Widgets.IconView (versions);
				
				// see bug #600032
				view.AddEvents ( (int) EventMask.KeyPressMask
					| (int) EventMask.KeyReleaseMask
					| (int) EventMask.PointerMotionMask
					| (int) EventMask.ButtonPressMask
					| (int) EventMask.ButtonReleaseMask);
				
				if (selected_version_id >= 0)
					view.Selection.Add (selected_version_id);
				view.Selection.Changed += OnVersionClicked;
				view.DisplayDates = false;
				view.DisplayTags = false;
				view.DisplayRatings = false;
				view.DisplayFilenames = true;
				thumb_scrolledwindow.Add (view);
				ShowAll ();
			} catch (Exception e) {
				Log.Exception (e);
			}
		}
		
		void OnVersionClicked (IBrowsableCollection collection)
		{
			if (collection.Count != 1 || SelectedItem == null) {
				return;
			}
			
			PhotoVersion selected_item = (PhotoVersion) collection.Items[0];
			
			FSpot.Photo photo = (FSpot.Photo) SelectedItem;
			uint [] version_ids = photo.VersionIds;
	
			foreach (uint id in version_ids) {
				if (((PhotoVersion)photo.GetVersion (id)).Uri == selected_item.Uri) {
					Log.Information (string.Format ("[VersionSidebar] Clicked on version id {0}.", id));
					if(photo.DefaultVersionId == id)
						return;
					photo.DefaultVersionId = id;
					App.Instance.Database.Photos.Commit (photo);
					return;
				}
			}
		}
		
		void ClearPhotoVersionViews ()
		{
			foreach (Widget w in thumb_scrolledwindow) {
				thumb_scrolledwindow.Remove (w);
			}
		}
	}
}
