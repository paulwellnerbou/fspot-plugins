/*
 * DevelopInUFraw.cs
 *
 * Author(s)
 * 	Stephane Delcroix  <stephane@delcroix.org>
 *
 * This is free software. See COPYING for details
 */

using System;
using System.IO;

using Mono.Unix;

using Hyena;
using FSpot;
using FSpot.Utils;
using FSpot.Extensions;
using FSpot.Imaging;
using FSpot.UI.Dialog;

namespace DevelopInBibble5Extension
{
	public class DevelopInBibble5 : ICommand {
		public DevelopInBibble5()
		{
		}
		
		// The executable used for developing RAWs
		private string executable = "bibble5";
		
		private string command_args = "";
		
		public void Run (object o, EventArgs e)
		{
			try {
	            Hyena.Log.Information ("[DevelopInBibble5] Executing extension");
	
				command_args = "";
				foreach (Photo p in App.Instance.Organizer.SelectedPhotos ())
					AddToCommand (p);
				
				Log.Debug (executable + " " + command_args);
				System.Diagnostics.Process.Start (executable, command_args);
			} catch (Exception ex) {
				Hyena.Log.Exception (ex);
                Log.Error ("[DevelopInBibble5] Bibble 5 installed?");
			}
		}

		protected void AddToCommand (Photo p)
		{
		    PhotoVersion raw = p.GetVersion (Photo.OriginalVersionId) as PhotoVersion;
		    if (!ImageFile.IsRaw (raw.Uri)) {
		        Log.Warning ("[DevelopInBibble5] The original version of this image is not a (supported) RAW file, opening anyway.");
		        // return;
		    }

			command_args += String.Format (" {0}", GLib.Shell.Quote (raw.Uri.LocalPath));
		}
	}
}
