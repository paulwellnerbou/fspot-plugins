/*
 * ImportRawProcessed.cs
 *
 * Author(s)
 * 	Paul Wellner Bou <paul@purecodes.org>
 *
 * This is free software. See COPYING for details
 */

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

using Mono.Unix;

using Hyena;
using FSpot;
using FSpot.Utils;
using FSpot.Extensions;

namespace ImportRawProcessedExtension
{
	public class ImportRawProcessed : ICommand
	{
		public void Run (object o, EventArgs e)
		{
			Log.Information ("[ImportRawProcessed] Importing processed files...");
			
			// search processed version of selected files
			foreach (Photo p in App.Instance.Organizer.SelectedPhotos ()) {
				
				SafeUri uri = p.DefaultVersion.Uri;
				string dir = System.IO.Path.GetDirectoryName (uri.AbsolutePath);
				string name_without_ext = System.IO.Path.GetFileNameWithoutExtension (p.Name);
				
				DirectoryInfo dir_info = new DirectoryInfo(dir);
				FileInfo[] files_in_dir = dir_info.GetFiles();
				
				foreach(FileInfo file in files_in_dir) {
					if (file.Name.Equals (p.Name))
						continue;
					
					if (file.Name.ToUpper ().StartsWith(name_without_ext.ToUpper ())) {
						// TODO: use TagLib's HasLoader
						if (!(file.Extension.Equals (".jpg") || file.Extension.Equals (".JPG") ||
						   file.Extension.Equals (".TIF") || file.Extension.Equals (".tif") || file.Extension.Equals (".gif") ||
						   file.Extension.Equals (".GIF") || file.Extension.Equals (".PNG") || file.Extension.Equals (".png"))) {
							Log.Information ("[ImportRawProcessed] Not importing " + file.FullName + ", no known Image format.");
							continue;
						}
						
						ImportProcessedVersion (p, file);
					}
				}
			}
			
			Log.Information ("[ImportRawProcessed] Done.");
		}
		
		private void ImportProcessedVersion (Photo p, FileInfo file) {
			foreach (PhotoVersion version in p.Versions) {
				if (version.Filename.Equals (file.Name))
					return;
			}
			
			int i = 0;
			string name_without_ext = System.IO.Path.GetFileNameWithoutExtension (file.Name);
			string name = name_without_ext;
			while (true) {
				if (!p.VersionNameExists (name))
					break;
				name = string.Format ("{0} {1}", name_without_ext, i++);
			}
			
			Log.Information ("[ImportRawProcessed] Importing " + file.FullName + " as "+name);
			p.DefaultVersionId = p.AddVersion (new SafeUri (file.FullName).GetBaseUri (), file.Name, name);
			p.Changes.DataChanged = true;
			App.Instance.Database.Photos.Commit (p);
		}
	}
}
