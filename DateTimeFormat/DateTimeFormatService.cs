/*
 * DateTimeFormatService.cs
 *
 * Author(s)
 * 	Paul Wellner Bou <paul@purecodes.org>
 *
 * This is free software. See COPYING for details
 */

using System;

using Hyena;

using FSpot;
using FSpot.Extensions;
using FSpot.Utils;

namespace DateTimeFormatService
{
	public class DateTimeFormatService : IService
	{
		public const string SHORT_DATE_PATTERN = Preferences.APP_FSPOT + "ext/datetimeformat/shortdate";
		public const string LONG_TIME_PATTERN = Preferences.APP_FSPOT + "ext/datetimeformat/longtime";
		
		public bool Start()
		{
			uint timer = Log.InformationTimerStart ("Starting DateTimeFormatService");
			
			string short_date_format = Preferences.Get<string> (SHORT_DATE_PATTERN);
			string long_time_format = Preferences.Get<string> (LONG_TIME_PATTERN);
			
			System.Globalization.CultureInfo cultureInfo = (System.Globalization.CultureInfo) System.Globalization.CultureInfo.CurrentCulture.Clone ();
			
			if (short_date_format == null)
				Log.Information (string.Format("[DateTimeFormat] No value for {0} defined.", SHORT_DATE_PATTERN));
			else
				cultureInfo.DateTimeFormat.ShortDatePattern = short_date_format;
			
			if (long_time_format == null)
				Log.Information (string.Format("[DateTimeFormat] No value for {0} defined.", LONG_TIME_PATTERN));
			else
				cultureInfo.DateTimeFormat.LongTimePattern = long_time_format;
			
			if (short_date_format != null || long_time_format != null)
				System.Threading.Thread.CurrentThread.CurrentCulture = cultureInfo;
			
			Log.DebugTimerPrint (timer, "DateTimeFormatService startup took {0}");
			return true;
		}

		public bool Stop ()
		{
			uint timer = Log.InformationTimerStart ("Stopping DateTimeFormatService");
			Log.DebugTimerPrint (timer, "DateTimeFormatService shutdown took {0}");
			return true;
		}
	}
}

